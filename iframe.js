window.onload = () => {
 
function fullPath(el){
  var names = [];
  while (el.parentNode){
    if (el.id){
      names.unshift('#'+el.id);
      break;
    }else{
      if (el==el.ownerDocument.documentElement) names.unshift(el.tagName);
      else{
        for (var c=1,e=el;e.previousElementSibling;e=e.previousElementSibling,c++);
        names.unshift(el.tagName+":nth-child("+c+")");
      }
      el=el.parentNode;
    }
  }
  return names.join(" > ");
}

function getXPathForElement(element) {
  const idx = (sib, name) => sib 
      ? idx(sib.previousElementSibling, name||sib.localName) + (sib.localName == name)
      : 1;
  const segs = elm => !elm || elm.nodeType !== 1 
      ? ['']
      : elm.id && document.getElementById(elm.id) === elm
          ? [`id("${elm.id}")`]
          : [...segs(elm.parentNode), `${elm.localName.toLowerCase()}[${idx(elm)}]`];
  return segs(element).join('/');
}

function getElementByXPath(path) { 
  return (new XPathEvaluator()) 
      .evaluate(path, document.documentElement, null, 
                      XPathResult.FIRST_ORDERED_NODE_TYPE, null) 
      .singleNodeValue; 
} 
  
  
  document.addEventListener('mouseover', (e) => {
    e.stopPropagation();
    e.target.classList.add('hovered');
  });
  document.addEventListener('mouseout', (e) => {
    e.stopPropagation();
    e.target.classList.remove('hovered');
  });

  document.addEventListener('click', (e) => {
    e.stopPropagation();
    e.preventDefault();
    alert(fullPath(e.target))
  })
}