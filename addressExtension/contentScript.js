(() => {
  // @ts-check
  function goToUrl() {
    if (addressInput.value.includes('http')) {
      window.location.href = addressInput.value;
    } else {
      window.location.href = 'https://'.concat(addressInput.value);
    }
  };
  
  const body = document.body;
  const addressBar = document.createElement('div');
  addressBar.classList.add('custom-address-bar');

  const addressInput = document.createElement('input');
  addressInput.value = window.location.href;
  addressInput.classList.add('custom-address-input');

  const playButton = document.createElement('img');
  playButton.classList.add('custom-button');
  playButton.src = 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACQAAAAkCAYAAADhAJiYAAAAqUlEQVR4Ae3VEQwCcRiG8eAgCIPwMAzPw5zCfPny5cuXL8wX5ssXhEEQBAfXA2cfHNzt3he+Z/v5u9X/u0mWZVl3Jaz6Yo8CFjWtO1Y+g4AaR0y1g6In1vpB0Rlzn0HAG1v9oOiK0mkQ4olQD4onQjlorBPR9HDBwmHQCxuHn6zGCTOHP/UDlcOz/+GAwuEw3rB0+HR8sIOgjqcsKDxlbfEp66swVFmWZX8K/6tRkuUooAAAAABJRU5ErkJggg==';

  const exitButton = document.createElement('img');
  exitButton.src = 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACQAAAAkCAYAAADhAJiYAAAAmElEQVR4Ae3TgQXAMBRF0Y8O1D2KoINklD9IRwzSIEDDJak8+JenQB2JWDQuiqJz3z+43Fb7dzZvK223LXZ1TCUUYGpfWT2po+1hFGP63KAFlABDKAGGUQIMowQYRgFGgAKMEuUmzAegLMdIUIBxeH17MfD6JBgToAADKAVmAwowClSawQDqtMWcMYz6+9qScYS6LIqi6NsLO/6TAUDU7/4AAAAASUVORK5CYII=';
  exitButton.classList.add('custom-button');

  playButton.onclick = goToUrl;
  addressInput.onkeypress = function(e) {
    if(e.keyCode === 13) {
      goToUrl();
    }
  }

  exitButton.onclick = function() {
    window.close();
  }
  addressBar.appendChild(addressInput);
  addressBar.appendChild(playButton);
  addressBar.appendChild(exitButton);
  
  
  // shift absolute positioned elements
  // if (body.childNodes.length > 1) {
  //   body.childNodes.forEach(node => {
      
  //     if (getComputedStyle(node).position === 'absolute' && getComputedStyle(node).top === '0px') {
  //       console.log(node);
        
  //       node.style.top = '32px'; // height of custom address bar
  //     }
  //   });
  // }
  body.prepend(addressBar);
})();