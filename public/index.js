// @ts-check

window.onload = () => {

  const iframeElement = document.getElementById('ifr');
  
  

  window.handleSubmit = function handleSubmit(e) {
    e.preventDefault();
    const { target: { elements: { iframe, url } } } = e;
    if (iframe.checked) {
      iframe.onload = () => {
        console.log('IFRAME loADED');
      }
      // console.log(window.frames['ifr'].contentWindow);
      iframeElement.contentWindow.location.replace(encodeURI(`/load?site=${url.value}`));
      
      return;
    }
    fetch('/open').then(() => {
      console.log('Browser opened');
    })
  };

};