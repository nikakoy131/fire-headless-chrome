const puppeteer = require('puppeteer');
const express = require('express');
const bodyParser = require('body-parser');
const morgan = require('morgan');
const jsdom = require("jsdom");
const path = require('path');
const fs = require('fs');
const mainCss = fs.readFileSync(path.normalize(__dirname + "/iframe.css"), 'utf8');
const mainJs = fs.readFileSync(path.normalize(__dirname + "/iframe.js"), 'utf8');
const { JSDOM } = jsdom;

const app = express();

const PORT = process.env.PORT || 3001;

app.use(express.static('public'));
app.use(morgan('tiny'));

// parse application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({ extended: false }))
 
// parse application/json
app.use(bodyParser.json())


app.get('/open', async (req, res) => {
  try {
    const pathToExtension = require('path').join(__dirname, 'addressExtension');
    const browser = await puppeteer.launch({
      headless: false,
      defaultViewport: null,
      args: [
        '--app=https://google.com',
        // '--auto-open-devtools-for-tabs',
        // '--kiosk=https://google.com',
        `--disable-extensions-except=${pathToExtension}`,
        `--load-extension=${pathToExtension}`
      ]
    });

    browser.on('targetdestroyed', async () => {
      console.log('targetdestroyed event');
      try {
        const pages = await browser.pages();
        if(!pages.length){
          await browser.close();
        }
      } catch (error) {
        console.log('browser closed early');
      }
    });

    const dimensions = await page.evaluate(() => {
      return {
        width: document.documentElement.clientWidth,
        height: document.documentElement.clientHeight,
        // deviceScaleFactor: window.devicePixelRatio
      };
    });
    // await page.setViewport({height: 1020, width: 960});

    res.send('Opened');
  } catch (err) {
    res.send(err.message);
  }
});

app.get('/load', async (req, res) => {
  const {site} = req.query;
  if (!site) {
    return res.sendStatus(400);
  }
  const browser = await puppeteer.launch({
    headless: true,
    // defaultViewport: null,
    args: [
      `--app=about:blank`,
    ]
  });
  const [page] = await browser.pages();
  // console.log(pages.length);
  await page.goto(site);

  // await page.waitFor({
  //   waitUntil: 'domcontentloaded',
  // });
  await page.screenshot({path: 'example.png'});

  const pageHtml = await page.evaluate(() => document.documentElement.outerHTML);

  const { document: JSDocument } = (new JSDOM(pageHtml)).window;
  const styles = JSDocument.createElement('style');
  const js = JSDocument.createElement('script');
  js.innerHTML = mainJs;
  styles.innerHTML = mainCss;
  JSDocument.head.appendChild(styles);
  JSDocument.head.appendChild(js);
  JSDocument.querySelector
  
  

  res.type('html');
  res.send(JSDocument.documentElement.outerHTML);
})

app.listen(PORT, () => {
  console.log('Server is running on port - ', PORT);
});
